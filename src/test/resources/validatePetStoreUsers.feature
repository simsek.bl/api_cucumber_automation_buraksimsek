@Regression
Feature: Validating the swagger USER API CRUD operation

  @ApiUser
  Scenario: Validating the swagger API CRUD operation
    Given As a user, I should create a new petStore account
      | id         | 3                     |
      | username   | ChuckNorris           |
      | firstName  | Chuck                 |
      | lastName   | Norris                |
      | email      | chucknorris@gmail.com |
      | password   | chuck1234             |
      | phone      | (222) 333-4455        |
      | userStatus | 4                     |
    Then Validate that the status code is 200