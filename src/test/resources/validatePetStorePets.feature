@Regression
Feature: Validating the swagger PET API CRUD operation

  @ApiPet
  Scenario Outline: Validating the swagger API CRUD operation
    Given As a owner, I should add new pet to the store with the below data
      | petId        | 3         |
      | categoryId   | 3         |
      | categoryName | cat       |
      | petName      | garfield  |
      | photoUrls    | cat_jp    |
      | tagsId       | 3         |
      | tagsName     | cute cat  |
      | status       | available |
    And Validate that status code is 200
    And Updating my pet with the following data "<categoryName>", "<petName>", "<tagsName>"
    And Validate that status code is 200
    And Make GET call to get user with 3 and validate "<status>"
    And Validate request body matches with the response body
    Then I delete user
    Examples: Updating pet data
      | categoryName | petName | tagsName | status    |
      | Squirrel     | Scrat   | Ice Age  | available |