package stepDefs;

import com.jayway.jsonpath.JsonPath;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pojoClasses.pets.Category;
import pojoClasses.pets.CreatePet;
import pojoClasses.pets.Tag;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static utils.APIUtils.serializePOJO;
import static utils.ConfigReader.getProperty;

public class PetsStepDefs {


    Response response;
    CreatePet createPet;
    Category category;
    Tag tag;
    int actualStatusCode;
    int actualId;
    int actualCategoryId;
    String actualCategoryName;
    String actualName;
    String actualPhotoUrl;
    int actualTagsId;
    String actualTagsName;
    String actualStatus;

    private static Logger logger = LogManager.getLogger(PetsStepDefs.class);

    @Given("As a owner, I should add new pet to the store with the below data")
    public void createPetWith(Map<String, String> petData) {

        category = Category.builder().id(Integer.parseInt(petData.get("categoryId")))
                .name(petData.get("categoryName")).build();
        tag = Tag.builder().id(Integer.parseInt(petData.get("tagsId")))
                .name(petData.get("tagsName")).build();

        createPet = CreatePet
                .builder().id(Integer.parseInt(petData.get("petId"))).name(petData.get("petName"))
                .category(category)
                .photoUrls(Collections.singletonList(petData.get("photoUrls")))
                .tags(Collections.singletonList(tag)).status(petData.get("status"))
                .build();


        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(serializePOJO(createPet))
                .post(getProperty("petsURL"))
                .then().log().all().extract().response();
    }

    @And("Validate that status code is {int}")
    public void validateThatStatusCodeIs(int expectedStatusCode) {

        actualStatusCode = response.getStatusCode();

        logger.debug("The expected petId should be " + expectedStatusCode + " we found " + actualStatusCode);
        logger.error("The expected petId should be " + expectedStatusCode + " we found " + actualStatusCode);
        assertThat(
                "I am expecting status code: " + expectedStatusCode,
                actualStatusCode,
                is(expectedStatusCode)
        );

    }

    @And("Updating my pet with the following data {string}, {string}, {string}")
    public void updatingMyPetWithTheFollowingData(String categoryName, String petName, String tagsName) {

        category = Category.builder().id(category.getId())
                .name(categoryName).build();
        tag = Tag.builder().id(tag.getId())
                .name(tagsName).build();

        createPet = CreatePet
                .builder().id(createPet.getId()).name(petName)
                .category(category)
                .photoUrls(createPet.getPhotoUrls())
                .tags(Collections.singletonList(tag)).status(createPet.getStatus())
                .build();


        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(serializePOJO(createPet))
                .put(getProperty("petsURL"))
                .then().log().all().extract().response();

    }

    @And("Make GET call to get user with {int} and validate {string}")
    public void makeGETCallToGetUserWith(int id, String status) {

        logger.debug("The expected petId should be " + status + " we found " + createPet.getStatus());
        logger.error("The expected petId should be " + status + " we found " + createPet.getStatus());
        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .get(getProperty("petsURL") + id)
                .then().log().all().assertThat()
                .body("status",equalTo("available"))
                .extract().response();
    }

    @And("Validate request body matches with the response body")
    public void validateRequestBodyMatchesWithTheResponseBody() {

        actualId = JsonPath.read(response.asString(), "id");
        actualCategoryId = JsonPath.read(response.asString(), "category.id");
        actualCategoryName = JsonPath.read(response.asString(), "category.name");
        actualName = JsonPath.read(response.asString(), "name");
        actualTagsId = JsonPath.read(response.asString(), "tags[0].id");
        actualTagsName = JsonPath.read(response.asString(), "tags[0].name");
        actualPhotoUrl = JsonPath.read(response.asString(), "photoUrls[0]");
        actualStatus = JsonPath.read(response.asString(), "status");

        Object[] responseBody = {actualId, actualCategoryId, actualCategoryName, actualName, actualPhotoUrl, actualTagsId, actualTagsName, actualStatus};
        Object [] requestBody = {createPet.getId(), category.getId(), category.getName(), createPet.getName(), createPet.getPhotoUrls().get(0), tag.getId(), tag.getName(), createPet.getStatus()};

        logger.debug("The expected should be: " + Arrays.toString(requestBody) + "\n but we found " + Arrays.toString(responseBody));
        logger.error("The expected should be: " + Arrays.toString(requestBody) + " \n but we found " + Arrays.toString(responseBody));
        for (int i = 0; i <responseBody.length; i++) {
            assertThat(
                    "Validating expected and actual: " + requestBody[i],
                    responseBody[i],
                    is(requestBody[i])
            );
        }
    }

    @Then("I delete user")
    public void iDeleteUser() {

        actualStatusCode = response.getStatusCode();

        logger.debug("The expected petId should be " + 200 + " we found " + actualStatusCode);
        logger.error("The expected petId should be " + 200 + " we found " + actualStatusCode);
        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .delete(getProperty("petsURL") + 3)
                .then().log().all().assertThat().statusCode(202).extract().response();
    }

}