package stepDefs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pojoClasses.users.CreateUser;

import java.util.Collections;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.APIUtils.serializePOJO;
import static utils.ConfigReader.getProperty;

public class UserStepDefs {

    Response response;
    CreateUser createUser;
    int actualStatusCode;

    private static Logger logger = LogManager.getLogger(PetsStepDefs.class);

    @Given("As a user, I should create a new petStore account")
    public void asAUserIShouldCreateANewPetStoreAccount(Map<String, String> petData) {
        createUser = CreateUser.builder().id(Integer.parseInt(petData.get("id")))
                .username(petData.get("username")).firstName(petData.get("firstName"))
                .lastName(petData.get("lastName")).email(petData.get("email"))
                .password(petData.get("password")).phone(petData.get("phone"))
                .userStatus(Integer.parseInt(petData.get("userStatus"))).build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(serializePOJO(Collections.singletonList(createUser)))
                .post(getProperty("petsURLUser"))
                .then().log().all().extract().response();
    }

    @Then("Validate that the status code is {int}")
    public void validateThatTheStatusCodeIs(int expectedStatusCode) {

        actualStatusCode = response.getStatusCode();

        logger.debug("The expected petId should be " + expectedStatusCode + " we found " + actualStatusCode);
        logger.error("The expected petId should be " + expectedStatusCode + " we found " + actualStatusCode);
        assertThat(
                "I am expecting status code: " + expectedStatusCode,
                actualStatusCode,
                is(expectedStatusCode)
        );
    }
}
